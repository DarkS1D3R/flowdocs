﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FlowDocs.Startup))]
namespace FlowDocs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
