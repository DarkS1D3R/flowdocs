﻿using FlowDocs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlowDocs.ViewModels
{
    public class UploadFileViewModel
    {
        
        public repositories myRepositories { get; set; }
        [Required]
        [Display(Name = "File")]
        [ValidateFile]
        public HttpPostedFileBase fileBase { get; set; }
    }

    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 * 3;

            string[] AllowedFileExtensions = new string[] { ".docx", ".xlsx", ".doc", ".csv", ".txt" };

            var file = value as HttpPostedFileBase;

            if (file == null)
            {
                return false;
            }
            else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
            {
                ErrorMessage = "Upload File must be of type: " + string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.ContentLength > MaxContentLength)
            {
                ErrorMessage = "The Size of the file is too large : " + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }
    }
}