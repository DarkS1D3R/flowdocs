﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlowDocs.Models;

namespace FlowDocs.ViewModels
{
    public class DashboardViewModel
    {
        public List<repositories> lstRepos { get; set; }
        public List<repositories> lstSharedRepos { get; set; }
        public repositories myRepositories { get; set; }
        public shared myshared { get; set; }
    }
}