﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlowDocs.Models;
using FlowDocs.Controllers;
using System.ComponentModel.DataAnnotations;

namespace FlowDocs.ViewModels
{
    public class RepositoryViewModel
    {
        public List<file> files { get; set; }
        
        public RepositoriesController myRepositoriesController { get; set; }

        public repositories myRepositories { get; set; }
        
    }
}