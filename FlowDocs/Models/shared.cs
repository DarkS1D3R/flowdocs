﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlowDocs.Models
{
    public class shared
    {
        [Key]
        public int srepoid { get; set; }
        
        [Display(Name = "Shared with")]
        public string users { get; set; }
    }
}