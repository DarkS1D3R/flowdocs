﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FlowDocs.Models
{
    public class repositories
    {       
        [Key]
        public int repoid { get; set; }

        [Required(ErrorMessage ="Please enter repository name.")]
        [Display(Name = "Repository Name")]
        public string reponame { get; set; }

       
        public string repopath { get; set; }

        public bool isshared { get; set; } 
        [Display(Name = "Emails of users shared with (Seperated by comas)")]
        public string sUsers { get; set; }

        public List<string> sharedUsers { get; set; }
        
        public ICollection<file> files { get; set; }
       
        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        
        public virtual ApplicationUser ApplicationUser { get; set; }


    }
}