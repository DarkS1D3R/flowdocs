﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlowDocs.Models
{
    public class file
    {
        [Key]
        public int fid { get; set; }
        [Required]
        public string filename { get; set; }
        [Required]
        public string filepath { get; set; }
        [Required]
        public string ftype { get; set; }

        public DateTime fdate { get; set; }

        
        public int repoid { get; set; }
        public repositories frepositories { get; set; }
        
        
    }
}