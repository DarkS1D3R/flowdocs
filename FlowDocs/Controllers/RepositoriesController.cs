﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlowDocs.Models;
using FlowDocs.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace FlowDocs.Controllers
{
    //enable disable authorization on repositories controller
    [Authorize]
    public class RepositoriesController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private static int repoId;
        

        public RepositoriesController()
        {
            _dbContext = new ApplicationDbContext();           
        }

        protected override void Dispose(bool disposing)
        {
            _dbContext.Dispose();
        }

        // GET: Repositories
        public ActionResult Dashboard()
        { 
            string userID = User.Identity.GetUserId();
            ApplicationUser myUser = _dbContext.Users.FirstOrDefault(x => x.Id == userID);

            List<repositories> listShared = new List<repositories>();
            List<repositories> repos = _dbContext.repositories.ToList();

            foreach(var myRepo in repos)
            {
                if(myRepo.sUsers != null)
                {
                    string trimmedUsers = myRepo.sUsers.Replace(" ", string.Empty);
                    string[] sUsers = trimmedUsers.Split(',');
                    for (int i = 0; i < sUsers.Length; i++)
                    {
                        if (sUsers[i] == myUser.Email)
                        {
                            listShared.Add(myRepo);
                        }
                    }
                }
                
            }

            var viewModel = new DashboardViewModel
            {
                lstRepos = _dbContext.repositories.Where(u => u.UserId == userID).ToList(),
                lstSharedRepos = listShared
                
            };          

            return View(viewModel);

        }

        //Helper action to return to Dashboard page
        public ActionResult returnToDash()
        {
            return RedirectToAction("Dashboard", "Repositories");
        }

        //Initialize repository page
        public ActionResult Repository(int Rid)
        {

            repoId = Rid;

            var viewModel = new RepositoryViewModel
            {
                files = _dbContext.files.Where(f => f.repoid == Rid).ToList()


            };
            viewModel.myRepositories = _dbContext.repositories.Where(x => x.repoid == Rid).FirstOrDefault();

            return View(viewModel);
        }

        //create a new repository
        [HttpPost]
        public ActionResult CreateRepo(AddRepositoryViewModel viewModel)
        {
            string userId = User.Identity.GetUserId();
            viewModel.myRepositories.UserId = userId;

            var myRepoPath = ("~/Content/Files/" + viewModel.myRepositories.reponame);
            viewModel.myRepositories.repopath = myRepoPath;

            if (!ModelState.IsValid)
            {
                var myViewModel = new AddRepositoryViewModel();
                myViewModel = viewModel;
                return View("AddRepository", myViewModel);
            }
            
            Directory.CreateDirectory(Server.MapPath(myRepoPath));

            _dbContext.repositories.Add(viewModel.myRepositories);
            _dbContext.SaveChanges();

            return RedirectToAction("Dashboard", "Repositories");
        }

        //upload a file to the specific repository
        [HttpPost]
        public ActionResult Uploadfile( UploadFileViewModel viewModel)
        {
            file myFile = new file();

            if (!ModelState.IsValid)
            {
                var myViewModel = new UploadFileViewModel();
                myViewModel = viewModel;
                return View("AddFile", myViewModel);
            }

            if (viewModel.fileBase != null && viewModel.fileBase.ContentLength > 0)
            {
                viewModel.myRepositories = _dbContext.repositories.Where(x => x.repoid == repoId).FirstOrDefault();
                var myRepoName = viewModel.myRepositories.reponame;

                var fileName = Path.GetFileName(viewModel.fileBase.FileName);
                var fileNameWE = Path.GetFileNameWithoutExtension(viewModel.fileBase.FileName);
                var fileExtension = Path.GetExtension(viewModel.fileBase.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Files/" + myRepoName), fileName);

                DateTime myDateTime = new DateTime();
                myDateTime = DateTime.Now;

                if (System.IO.File.Exists(path))
                {

                    var newFileName = fileNameWE + " - " + myDateTime.ToFileTime() + fileExtension;
                    
                    var newPath = Path.Combine(Server.MapPath("~/Content/Files/" + myRepoName), newFileName);
                    viewModel.fileBase.SaveAs(newPath);

                    myFile.filename = newFileName;
                    myFile.filepath = newPath;
                    myFile.ftype = Path.GetExtension(newPath);
                }
                else
                {
                    viewModel.fileBase.SaveAs(path);

                    myFile.filename = fileName;
                    myFile.filepath = path;
                    myFile.ftype = Path.GetExtension(path);
                }                  

                
                myFile.fdate = myDateTime;
                myFile.repoid = repoId;
            }
            else
            {
                return HttpNotFound();
            }

            _dbContext.files.Add(myFile);
            _dbContext.SaveChanges();
            return RedirectToAction("Repository", "Repositories", new { Rid = repoId });
        }

        //Download a file when clicked on
        public FilePathResult DownloadFile(string fname, RepositoryViewModel viewModel)
        {
            viewModel.myRepositories = _dbContext.repositories.Where(x => x.repoid == repoId).FirstOrDefault();
            var myRepoName = viewModel.myRepositories.reponame;

            var virtualFilePath = "~/Content/Files/" + myRepoName + "/" + fname;
            return File(virtualFilePath, "application/force-download", Path.GetFileName(virtualFilePath));
            
        }  
        //Delete file on database and locally       
        public ActionResult DeleteFile(int id, RepositoryViewModel viewModel)
        {
            viewModel.myRepositories = _dbContext.repositories.Where(x => x.repoid == repoId).FirstOrDefault();
            var myRepoName = viewModel.myRepositories.reponame;

            file myFile = _dbContext.files.Find(id);
            var fileName = myFile.filename;
            

            var virtualFilePath = Request.MapPath("~/Content/Files/" + myRepoName + "/" + fileName);
            if (System.IO.File.Exists(virtualFilePath))
            {
                System.IO.File.Delete(virtualFilePath);
            }
            else
                return HttpNotFound();

            _dbContext.files.Remove(myFile);
            _dbContext.SaveChanges();

            return RedirectToAction("Repository", "Repositories", new { Rid = repoId });
        }

        public ActionResult AddRepository()
        {
            return View();
        }

        public ActionResult AddFile()
        {
            return View();
        }

        //Helper action to return to Repository page
        public ActionResult returnToRepositories()
        {
            return RedirectToAction("Repository", "Repositories", new { Rid = repoId });
        }

        //remove
        public ActionResult sharedEdit(AddRepositoryViewModel viewModel)
        {
            if (viewModel.myRepositories.sUsers == null || viewModel.myRepositories.sUsers == "")
            {
                viewModel.myRepositories.isshared = false;
            }
            else
            {
                viewModel.myRepositories.isshared = true;
            }

            if (ModelState.IsValid)
            {
                _dbContext.Entry(viewModel.myRepositories).State = System.Data.Entity.EntityState.Modified;
                _dbContext.SaveChanges();
            }
            else
                return HttpNotFound();

            return RedirectToAction("Dashboard", "Repositories", new { Rid = repoId });
        }

        public ActionResult EditShared(int myRid)
        {
            var viewModel = new AddRepositoryViewModel
            {                
                myRepositories = _dbContext.repositories.Where(x => x.repoid == myRid).FirstOrDefault()
            };
            return View(viewModel);
        }
       
    }
}