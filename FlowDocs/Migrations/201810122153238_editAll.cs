namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editAll : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.users",
                c => new
                    {
                        userid = c.Int(nullable: false, identity: true),
                        fname = c.String(nullable: false),
                        lname = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.userid);
            
            AlterColumn("dbo.files", "filename", c => c.String(nullable: false));
            AlterColumn("dbo.files", "filepath", c => c.String(nullable: false));
            AlterColumn("dbo.files", "ftype", c => c.String(nullable: false));
            AlterColumn("dbo.repositories", "reponame", c => c.String(nullable: false));
            AlterColumn("dbo.repositories", "repopath", c => c.String(nullable: false));
            AlterColumn("dbo.shareds", "users", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.shareds", "users", c => c.String());
            AlterColumn("dbo.repositories", "repopath", c => c.String());
            AlterColumn("dbo.repositories", "reponame", c => c.String());
            AlterColumn("dbo.files", "ftype", c => c.String());
            AlterColumn("dbo.files", "filepath", c => c.String());
            AlterColumn("dbo.files", "filename", c => c.String());
            DropTable("dbo.users");
        }
    }
}
