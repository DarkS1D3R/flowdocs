namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixForeignKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.files", "frepositories_repoid", "dbo.repositories");
            DropForeignKey("dbo.shareds", "srepositories_repoid", "dbo.repositories");
            DropIndex("dbo.files", new[] { "frepositories_repoid" });
            DropIndex("dbo.shareds", new[] { "srepositories_repoid" });
            RenameColumn(table: "dbo.files", name: "frepositories_repoid", newName: "repoid");
            RenameColumn(table: "dbo.shareds", name: "srepositories_repoid", newName: "repoid");
            AlterColumn("dbo.files", "repoid", c => c.Int(nullable: false));
            AlterColumn("dbo.shareds", "repoid", c => c.Int(nullable: false));
            CreateIndex("dbo.files", "repoid");
            CreateIndex("dbo.shareds", "repoid");
            AddForeignKey("dbo.files", "repoid", "dbo.repositories", "repoid", cascadeDelete: true);
            AddForeignKey("dbo.shareds", "repoid", "dbo.repositories", "repoid", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.shareds", "repoid", "dbo.repositories");
            DropForeignKey("dbo.files", "repoid", "dbo.repositories");
            DropIndex("dbo.shareds", new[] { "repoid" });
            DropIndex("dbo.files", new[] { "repoid" });
            AlterColumn("dbo.shareds", "repoid", c => c.Int());
            AlterColumn("dbo.files", "repoid", c => c.Int());
            RenameColumn(table: "dbo.shareds", name: "repoid", newName: "srepositories_repoid");
            RenameColumn(table: "dbo.files", name: "repoid", newName: "frepositories_repoid");
            CreateIndex("dbo.shareds", "srepositories_repoid");
            CreateIndex("dbo.files", "frepositories_repoid");
            AddForeignKey("dbo.shareds", "srepositories_repoid", "dbo.repositories", "repoid");
            AddForeignKey("dbo.files", "frepositories_repoid", "dbo.repositories", "repoid");
        }
    }
}
