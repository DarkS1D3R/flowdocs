namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createAssociative : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.userrepositories",
                c => new
                    {
                        user_userid = c.Int(nullable: false),
                        repositories_repoid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.user_userid, t.repositories_repoid })
                .ForeignKey("dbo.users", t => t.user_userid, cascadeDelete: true)
                .ForeignKey("dbo.repositories", t => t.repositories_repoid, cascadeDelete: true)
                .Index(t => t.user_userid)
                .Index(t => t.repositories_repoid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.userrepositories", "repositories_repoid", "dbo.repositories");
            DropForeignKey("dbo.userrepositories", "user_userid", "dbo.users");
            DropIndex("dbo.userrepositories", new[] { "repositories_repoid" });
            DropIndex("dbo.userrepositories", new[] { "user_userid" });
            DropTable("dbo.userrepositories");
        }
    }
}
