namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correctForeignKeyForAspUserRepositories : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.repositories", new[] { "myUserId_Id" });
            DropColumn("dbo.repositories", "UserId");
            RenameColumn(table: "dbo.repositories", name: "myUserId_Id", newName: "UserId");
            AlterColumn("dbo.repositories", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.repositories", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.repositories", new[] { "UserId" });
            AlterColumn("dbo.repositories", "UserId", c => c.String());
            RenameColumn(table: "dbo.repositories", name: "UserId", newName: "myUserId_Id");
            AddColumn("dbo.repositories", "UserId", c => c.String());
            CreateIndex("dbo.repositories", "myUserId_Id");
        }
    }
}
