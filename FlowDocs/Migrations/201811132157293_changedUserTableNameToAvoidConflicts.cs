namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedUserTableNameToAvoidConflicts : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.users", newName: "myUsers");
            RenameTable(name: "dbo.userrepositories", newName: "myUserrepositories");
            RenameColumn(table: "dbo.myUserrepositories", name: "user_userid", newName: "myUser_userid");
            RenameIndex(table: "dbo.myUserrepositories", name: "IX_user_userid", newName: "IX_myUser_userid");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.myUserrepositories", name: "IX_myUser_userid", newName: "IX_user_userid");
            RenameColumn(table: "dbo.myUserrepositories", name: "myUser_userid", newName: "user_userid");
            RenameTable(name: "dbo.myUserrepositories", newName: "userrepositories");
            RenameTable(name: "dbo.myUsers", newName: "users");
        }
    }
}
