namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class linkAspNetUsersToRepositories : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.repositories", "UserId", c => c.String());
            AddColumn("dbo.repositories", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.repositories", "ApplicationUser_Id");
            AddForeignKey("dbo.repositories", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.repositories", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.repositories", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.repositories", "ApplicationUser_Id");
            DropColumn("dbo.repositories", "UserId");
        }
    }
}
