namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeMyusersAndChangeRepositories : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.myUserrepositories", "myUser_userid", "dbo.myUsers");
            DropForeignKey("dbo.myUserrepositories", "repositories_repoid", "dbo.repositories");
            DropIndex("dbo.myUserrepositories", new[] { "myUser_userid" });
            DropIndex("dbo.myUserrepositories", new[] { "repositories_repoid" });
            DropTable("dbo.myUserrepositories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.myUserrepositories",
                c => new
                    {
                        myUser_userid = c.Int(nullable: false),
                        repositories_repoid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.myUser_userid, t.repositories_repoid });
            
            CreateIndex("dbo.myUserrepositories", "repositories_repoid");
            CreateIndex("dbo.myUserrepositories", "myUser_userid");
            AddForeignKey("dbo.myUserrepositories", "repositories_repoid", "dbo.repositories", "repoid", cascadeDelete: true);
            AddForeignKey("dbo.myUserrepositories", "myUser_userid", "dbo.myUsers", "userid", cascadeDelete: true);
        }
    }
}
