namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeSharedTable : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.shareds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.shareds",
                c => new
                    {
                        srepoid = c.Int(nullable: false, identity: true),
                        users = c.String(),
                    })
                .PrimaryKey(t => t.srepoid);
            
        }
    }
}
