namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editForeignKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.repositories", "file_fid", "dbo.files");
            DropForeignKey("dbo.repositories", "shared_srepoid", "dbo.shareds");
            DropIndex("dbo.repositories", new[] { "file_fid" });
            DropIndex("dbo.repositories", new[] { "shared_srepoid" });
            AddColumn("dbo.files", "frepositories_repoid", c => c.Int());
            AddColumn("dbo.shareds", "srepositories_repoid", c => c.Int());
            CreateIndex("dbo.files", "frepositories_repoid");
            CreateIndex("dbo.shareds", "srepositories_repoid");
            AddForeignKey("dbo.files", "frepositories_repoid", "dbo.repositories", "repoid");
            AddForeignKey("dbo.shareds", "srepositories_repoid", "dbo.repositories", "repoid");
            DropColumn("dbo.repositories", "file_fid");
            DropColumn("dbo.repositories", "shared_srepoid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.repositories", "shared_srepoid", c => c.Int());
            AddColumn("dbo.repositories", "file_fid", c => c.Int());
            DropForeignKey("dbo.shareds", "srepositories_repoid", "dbo.repositories");
            DropForeignKey("dbo.files", "frepositories_repoid", "dbo.repositories");
            DropIndex("dbo.shareds", new[] { "srepositories_repoid" });
            DropIndex("dbo.files", new[] { "frepositories_repoid" });
            DropColumn("dbo.shareds", "srepositories_repoid");
            DropColumn("dbo.files", "frepositories_repoid");
            CreateIndex("dbo.repositories", "shared_srepoid");
            CreateIndex("dbo.repositories", "file_fid");
            AddForeignKey("dbo.repositories", "shared_srepoid", "dbo.shareds", "srepoid");
            AddForeignKey("dbo.repositories", "file_fid", "dbo.files", "fid");
        }
    }
}
