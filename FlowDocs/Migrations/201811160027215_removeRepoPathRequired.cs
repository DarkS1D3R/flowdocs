namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRepoPathRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.repositories", "repopath", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.repositories", "repopath", c => c.String(nullable: false));
        }
    }
}
