namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeApplicationUserId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.repositories", name: "ApplicationUser_Id", newName: "myUserId_Id");
            RenameIndex(table: "dbo.repositories", name: "IX_ApplicationUser_Id", newName: "IX_myUserId_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.repositories", name: "IX_myUserId_Id", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.repositories", name: "myUserId_Id", newName: "ApplicationUser_Id");
        }
    }
}
