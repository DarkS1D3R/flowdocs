namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeSharedTableForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.shareds", "repoid", "dbo.repositories");
            DropIndex("dbo.shareds", new[] { "repoid" });
            AddColumn("dbo.repositories", "sUsers", c => c.String());
            DropColumn("dbo.shareds", "repoid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.shareds", "repoid", c => c.Int(nullable: false));
            DropColumn("dbo.repositories", "sUsers");
            CreateIndex("dbo.shareds", "repoid");
            AddForeignKey("dbo.shareds", "repoid", "dbo.repositories", "repoid", cascadeDelete: true);
        }
    }
}
