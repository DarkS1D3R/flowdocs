namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fileAdded : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.repositories", "fileid");
            DropColumn("dbo.repositories", "sharedid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.repositories", "sharedid", c => c.Int(nullable: false));
            AddColumn("dbo.repositories", "fileid", c => c.Int(nullable: false));
        }
    }
}
