// <auto-generated />
namespace FlowDocs.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class createAssociative : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(createAssociative));
        
        string IMigrationMetadata.Id
        {
            get { return "201810131003459_createAssociative"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
