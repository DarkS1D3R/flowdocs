namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.files",
                c => new
                    {
                        fid = c.Int(nullable: false, identity: true),
                        filename = c.String(),
                        filepath = c.String(),
                        ftype = c.String(),
                        fdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.fid);
            
            CreateTable(
                "dbo.repositories",
                c => new
                    {
                        repoid = c.Int(nullable: false, identity: true),
                        reponame = c.String(),
                        repopath = c.String(),
                        isshared = c.Boolean(nullable: false),
                        fileid = c.Int(nullable: false),
                        sharedid = c.Int(nullable: false),
                        file_fid = c.Int(),
                        shared_srepoid = c.Int(),
                    })
                .PrimaryKey(t => t.repoid)
                .ForeignKey("dbo.files", t => t.file_fid)
                .ForeignKey("dbo.shareds", t => t.shared_srepoid)
                .Index(t => t.file_fid)
                .Index(t => t.shared_srepoid);
            
            CreateTable(
                "dbo.shareds",
                c => new
                    {
                        srepoid = c.Int(nullable: false, identity: true),
                        users = c.String(),
                    })
                .PrimaryKey(t => t.srepoid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.repositories", "shared_srepoid", "dbo.shareds");
            DropForeignKey("dbo.repositories", "file_fid", "dbo.files");
            DropIndex("dbo.repositories", new[] { "shared_srepoid" });
            DropIndex("dbo.repositories", new[] { "file_fid" });
            DropTable("dbo.shareds");
            DropTable("dbo.repositories");
            DropTable("dbo.files");
        }
    }
}
