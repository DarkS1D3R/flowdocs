namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeUsersTable : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.myUsers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.myUsers",
                c => new
                    {
                        userid = c.Int(nullable: false, identity: true),
                        fname = c.String(nullable: false),
                        lname = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.userid);
            
        }
    }
}
