namespace FlowDocs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedRequiredOnUsersForShared : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.shareds", "users", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.shareds", "users", c => c.String(nullable: false));
        }
    }
}
